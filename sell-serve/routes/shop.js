const express = require("express");
const router = express.Router();
const multer = require("multer");

const conn = require("./db/conn");


const storage = multer.diskStorage({
  destination: "public/upload/shop", //
  filename: function (req, file, cb) {
    var fileFormat = file.originalname.split(".");
    var filename = new Date().getTime();
    cb(null, filename + "." + fileFormat[fileFormat.length - 1]);
  },
});
// 上传对象
const upload = multer({
  storage,
});

// 店铺上传
router.post("/upload", upload.single("file"), (req, res) => {
  let { filename } = req.file;
  res.send({ code: 0, msg: "上传成功!", imgUrl: filename });
});

/* 获取店铺数据 */
router.get("/info", (req, res) => {
  const sql = `select * from shop where id = 1`;
  conn.query(sql, (err, data) => {
    if (err) throw err;
    if (data.length) {
      for (let key in data[0]) {
        if (key === "date" || key === "supports" || key === "pics") {
          data[0][key] = JSON.parse(data[0][key]);
        }
      }
    }
    // console.log(data[0])
    // data[0].avatar = "http://127.0.0.1:5000/upload/shop/" + data[0].avatar
    res.send({ data: data[0] });
  });
});

/* 店铺修改 */
router.post("/edit", (req, res) => {
  let {
    id,
    name,
    bulletin,
    avatar,
    deliveryPrice,
    deliveryTime,
    description,
    score,
    sellCount,
    supports,
    pics,
    date,
  } = req.body;

  if (
    !(
      id &&
      name &&
      bulletin &&
      avatar &&
      deliveryTime &&
      description &&
      score &&
      sellCount &&
      supports &&
      pics &&
      date
    )
  ) {
    res.send({ code: 5001, msg: "参数错误!" });
    return;
  }

  const sql = `update shop set name="${name}", bulletin="${bulletin}", avatar="${avatar}", deliveryPrice=${deliveryPrice}, deliveryTime="${deliveryTime}", 
	description="${description}", score=${score}, sellCount=${sellCount}, supports='${supports}',pics='${pics}', date='${date}'`;

  conn.query(sql, (err, data) => {
    if (err) throw err;
    if (data.affectedRows > 0) {
      res.send({ code: 0, msg: "修改店铺信息成功" });
    } else {
      res.send({ code: 1, msg: "修改店铺信息失败" });
    }
  });
});

/* App端商家数据 */
router.get("/seller", (req, res) => {
  const sql = `select * from shop where id = 1`;
  conn.query(sql, (err, data) => {
    if (err) throw err;

    if (data.length) {
      for (let key in data[0]) {
        if (key === "date" || key === "supports" || key === "pics") {
          data[0][key] = JSON.parse(data[0][key]);
        }
      }
    }
    data[0].avatar = "http://127.0.0.1:5000/upload/shop/" + data[0].avatar;
    data[0].pics = data[0].pics.map(
      (v) => "http://127.0.0.1:5000/upload/shop/" + v
    );
    res.send({ data: data[0] });
  });
});

/* App端评价数据 */
router.get("/ratings", function (req, res) {
  let ratings = {
    errno: 0,
    data: [
      {
        username: "3******c",
        rateTime: "2022-03-28-19:53",
        deliveryTime: 30,
        score: 5,
        rateType: 0,
        text: "让味蕾绽放喜悦好的食材是关键经过超高压分子均质化后使分子烘焙粉运用在面团里有效延长了组织老化时间增加了产品的风味，是食物口感升级到殿堂的秘密武器，成就了香醇与细腻强烈推荐.",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",

        recommend: [
          "荔浦芋泥",
          "提拉米苏",
          "青柠绿妍 ",
          "桂花拿铁",
          "桃乌乌系列",
        ],
      },
      {
        username: "2******3",
        rateTime: "2022-02-28-12:53",
        deliveryTime: "",
        score: 4,
        rateType: 0,
        deliveryTime: "",
        text: "马妹的甜品店就是不一样，服务态度不错",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [
          "增加了产品的风味",
          "好的食材是关键",
          "经过超高压分子均质化后",
          "使分子烘焙粉运用在面团里"
        ],
      },
      {
        username: "3******b",
        rateTime: "2021-02-30-12:53",
        score: 3,
        rateType: 1,
        text: "",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [],
      },
      {
        username: "1******c",
        rateTime: "2021-09-30-12:23",
        deliveryTime: 20,
        score: 5,
        rateType: 0,
        text: "良心店铺",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [],
      },
      {
        username: "2******d",
        rateTime: "2021-10-30-12:53",
        deliveryTime: 10,
        score: 4,
        rateType: 0,
        text: "",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [],
      },
      {
        username: "9******0",
        rateTime: "2022-03-09-16:53",
        deliveryTime: 70,
        score: 1,
        rateType: 1,
        text: "送货速度蜗牛一样",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [],
      },
      {
        username: "d******c",
        rateTime: "2022-03-09-16:53",
        deliveryTime: 30,
        score: 5,
        rateType: 0,
        text: "很喜欢的粥店",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [],
      },
      {
        username: "2******3",
        rateTime: "2022-03-19-16:53",
        deliveryTime: "",
        score: 4,
        rateType: 0,
        text: "量给的还可以",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [],
      },
      {
        username: "3******8",
        rateTime: "2022-03-29-16:53",
        deliveryTime: "",
        score: 3,
        rateType: 1,
        text: "",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [],
      },
      {
        username: "a******a",
        rateTime: "2022-09-29-16:53",
        deliveryTime: "",
        score: 4,
        rateType: 0,
        text: "孩子喜欢吃这家",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: ["南瓜粥"],
      },
      {
        username: "3******3",
        rateTime: "2019-03-29-16:53",
        deliveryTime: "",
        score: 4,
        rateType: 0,
        text: "汉堡包挺好吃的",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [],
      },
      {
        username: "t******b",
        rateTime: "2019-09-29-16:53",
        deliveryTime: "",
        score: 3,
        rateType: 1,
        text: "",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [],
      },
      {
        username: "f******c",
        rateTime: "2019-09-23-16:23",
        deliveryTime: 15,
        score: 5,
        rateType: 0,
        text: "送货速度很快",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [],
      },
      {
        username: "k******3",
        rateTime: "2019-09-23-20:23",
        deliveryTime: "",
        score: 4,
        rateType: 0,
        text: "",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [],
      },
      {
        username: "u******b",
        rateTime: "2019-09-09-16:23",
        deliveryTime: "",
        score: 4,
        rateType: 0,
        text: "下雨天给快递小哥点个赞",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [],
      },
      {
        username: "s******c",
        rateTime: "2019-09-23-16:23",
        deliveryTime: "",
        score: 4,
        rateType: 0,
        text: "好",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [],
      },
      {
        username: "z******3",
        rateTime: "2019-09-23-16:23",
        deliveryTime: "",
        score: 5,
        rateType: 0,
        text: "吃了还想再吃",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [],
      },
      {
        username: "n******b",
        rateTime: "2019-09-23-16:23",
        deliveryTime: "",
        score: 3,
        rateType: 1,
        text: "发票开的不对",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [],
      },
      {
        username: "m******c",
        rateTime: "2019-09-23-16:23",
        deliveryTime: 30,
        score: 5,
        rateType: 0,
        text: "好吃",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [],
      },
      {
        username: "l******3",
        rateTime: "2019-09-23-16:23",
        deliveryTime: 40,
        score: 5,
        rateType: 0,
        text: "还不错吧",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [],
      },
      {
        username: "3******o",
        rateTime: "2019-09-23-16:23",
        deliveryTime: "",
        score: 2,
        rateType: 1,
        text: "",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [],
      },
      {
        username: "3******p",
        rateTime: "2019-09-23-16:23",
        deliveryTime: "",
        score: 4,
        rateType: 0,
        text: "很喜欢的粥",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [],
      },
      {
        username: "o******k",
        rateTime: "2019-09-23-16:23",
        deliveryTime: "",
        score: 5,
        rateType: 0,
        text: "",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [],
      },
      {
        username: "k******b",
        rateTime: "2019-09-23-16:23",
        deliveryTime: 10,
        score: 4,
        rateType: 0,
        text: "",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [],
      },
      {
        username: "m******b",
        rateTime: 2019 - 09 - 23 - 16 - 23,
        deliveryTime: "",
        score: 4,
        rateType: 1,
        text: "",
        avatar:
          "http://static.galileo.xiaojukeji.com/static/tms/default_header.png",
        recommend: [],
      },
    ],
  };
  res.send(ratings);
});

module.exports = router;
