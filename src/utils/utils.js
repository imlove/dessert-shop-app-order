// 公用函数的工具文件

//  将时间写成标准格式 <10则前面+0
var getNum = (num) => {
    return num < 10 ? "0" + num : num
}
// 1.获取时间的函数
export var getTime = (UTCtime) => {
    let date = typeof UTCtime == "string" ? new Date(UTCtime) : UTCtime
    return getNum(date.getFullYear()) + "-" + getNum((date.getMonth() + 1)) + "-" + getNum(date.getDate()) + " " + getNum(date.getHours()) + ":" + getNum(date.getMinutes()) + ":" + getNum(date.getSeconds())
}